<?php
session_start();
if(isset($_SESSION['login']) && !empty($_SESSION['login']))
{
    $log = "Log out";
    $email = $_SESSION['login'];
    include 'server.php';

    if (isset($_GET['PID'])) 
    {
        $postID =  $_GET['PID'];
    }
    if ($db_found) 
    {
        $SQL = "SELECT * FROM question WHERE (post_id = $postID)";  //load question
        $result = mysqli_query($db_handle, $SQL);
        $db_field = mysqli_fetch_assoc($result);
        
        $SQL_2 = "SELECT first_name, last_name, email, gender FROM user WHERE (email = $email)";
        $result2 = mysqli_query($db_handle, $SQL_2);    //load user Profile
        $db_field2 = mysqli_fetch_assoc($result2);
        $postTime = date("h : m : sa",strtotime($db_field['post_time']));
        $postDate = date("d  M  Y",strtotime($db_field['post_date']));

        $SQL_3 = "SELECT * FROM answer WHERE (questionId = $postID) ORDER BY answerDate DESC, answerTime DESC";   
        $result3 = mysqli_query($db_handle, $SQL_3);                //load answer

        $query = "SELECT * FROM question";
        $result1 = mysqli_query($db_handle, $query);
        $total_question = mysqli_num_rows($result1);

        $query2 = "SELECT * FROM question WHERE (user_email = $email)";
        $result4 = mysqli_query($db_handle, $query2);
        $user_total_question = mysqli_num_rows($result4); // to count total question by user

        $query3 = "SELECT * FROM answer";
        $result5 = mysqli_query($db_handle, $query3);
        $answer = mysqli_num_rows($result5);
    }
    mysqli_close($db_handle);
}
else
{
    header('Location: index.php');
}

function gettingname($value)
{
    include 'server.php';
    $value =  quote_smart($value,$db_handle);
    $SQL_4 = "SELECT first_name, last_name FROM user WHERE (email = $value)";
    $result4 = mysqli_query($db_handle, $SQL_4);    //load user Profile
    $db_field4 = mysqli_fetch_assoc($result4);
    return $db_field4['first_name']." ".$db_field4['last_name'];
}

function quote_smart($value, $handle) 
{
   if (get_magic_quotes_gpc()) {
       $value = stripslashes($value);
   }
   if (!is_numeric($value)) {
       $value = "'" . mysqli_real_escape_string($handle,$value) . "'";
   }
   return $value;
}
?>

<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/nav.css">
    <link rel="stylesheet" href="css/about.css">
    <link rel="stylesheet" href="css/profile.css">

    <title>Your Post</title>
</head>
<body>
<?php include "navbar.php"; ?>
<?php include "editquestion.php"; ?>

<section class="mainContent">
 <div class="container">
    <div class="thumbnail" style="border:none;">
        <div class="row">
            <div class="col-md-8">
                <div class="row">        
                  <div class="col-sm-2">
                      <?php if($db_field2['gender'] == 'Male'): ?> 
                    	<?php echo '<img alt="User Pic" src="picture/male.png" width="100px" height="100px" style="margin-left:15px;" class="img-fluid rounded"> '; ?>
                    <?php else: ?>
                    	<?php echo '<img src="picture/female.png" width="100px" height="100px" style="margin-left:15px;" class="img-fluid rounded">'; ?>
                    <?php endif; ?>
                    
                  </div>

                  <div class="col-sm-10">  
                    <h3 style="margin-left:15px;"><a href="profile.php"><b><?= $db_field2['first_name']." ".$db_field2['last_name'] ?></b></a></h3>
                    <h4 class="text-justify" style="color:#03225C; margin:5px 15px 10px 15px;"><?= $db_field['post_title'] ?></h4>
                        <h6 class="text-muted" style="margin-left:15px;"><?= $postTime." ".$postDate ?></h6>
                        <p class="text-justify" style="color:#03225C; margin:5px 15px 10px 15px;"><?= $db_field['post_question'] ?></p> 
                        <div>
                            <button class="btn btn-outline-dark btn-sm"data-toggle="modal" data-target="#editQuestion" style="margin:5px 0px 10px 15px;">Edit</button>

                            <button class="btn btn-outline-danger btn-sm" data-toggle="modal" data-target="#deleteQuestion" style="margin:5px 15px 10px 5px;">Delete</button>
                        </div>                   
                  </div>    
                </div>
                <hr>

                <!-- Answer Section-->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 style="color:#03225C;margin:5px 0px 10px 15px;">Most Recent Answers</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">                        
                        <div class="row" style="margin:5px 0px 10px 15px;">
                            <?php
                            while($db_field3 = $result3->fetch_assoc()): 
                                $useremail = gettingname($db_field3['userEmail']); ?>               
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-1">
                                    <img src="http://bootdey.com/img/Content/user_1.jpg" class="img-fluid rounded">
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-11"> 
                                    <h3 style="color:#03225C;margin-top:1px;"><b><?= $useremail ?></b></h3>
                                    <?php 
                                        $Time = date("h : m : sa",strtotime($db_field3['answerTime']));
                                        $Date = date("d  M  Y",strtotime($db_field3['answerDate']));?>
                                    <h6 class="text-muted"><?= $Time." ".$Date ?></h6>
                                    <p class="text-justify"><?= $db_field3['answerBody'] ?></p>
                                    <hr>
                                </div>
                            <?php endwhile; ?>
                        </div>

                        <form method="post" action="answerConfirm.php">
                            <input type="hidden" name="postId" value="<?= $postID ?>">
                            <div align="center">
                                <textarea name="answer" cols="40" rows="5" maxlength="20000" class="form-control" placeholder="<?= "Your answer...." ?>" style="color:#03225C; width: 95%;" required></textarea><br>
                                <button class="btn btn-outline-dark" type="submit">Submit Answer</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sideBar">
                  <h1 class="text-center">STATS</h1>
                  <hr class="sideBarHr">
                  <div class="sideBarStatBox">
                    <h3><i class="fas fa-question-circle"></i>Total Queestions (<?= $total_question?>)</h3>
                  </div>
                  <div class="sideBarStatBox">
                    <h3><i class="fas fa-reply"></i>Total Answers (<?= $answer ?>)</h3>
                  </div>
                  <div class="sideBarStatBox">
                    <h3><i class="fas fa-question"></i>Your asked <?= $user_total_question ?> Questions</h3>
                  </div>
                  <!-- <div class="sideBarStatBox">
                    <h3><i class="fas fa-thumbs-up"></i>Likes (200)</h3>
                  </div> -->
                </div>
            </div>
        </div>

        
    </div>
 </div>   
</section>



<?php
    include 'footer.php';
?>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>