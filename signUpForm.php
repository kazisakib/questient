<!--Signup Modal-->
<div class="modal fade" style="" id="signUpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-dialog-centered" style="overflow-y: initial;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">SignUP Here...</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="height: 400px; overflow-y: auto;">

        <form onsubmit="return checkPassword()" name="signUpForm" action="registration_confirm.php" method="post" class="form" role="form">
            <div class="row">
                <div class="col-sm-3 col-form-label">
                   <label class="h5">Name:</label>
                </div>
                <div class="col-sm-5">
                    <input type="text" class="form-control" placeholder="FirstName" name="firstname" required autofocus>
                </div>
                <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="LastName" name="lastname" required>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3 col-form-label">
                   <label class="h5">Email:</label>
                </div>
                <div class="col-sm-9">
                    <input type="email" class="form-control" placeholder="Enter your email" name="email" required>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3 col-form-label">
                   <label class="h5">Password:</label>
                </div>
                <div class="col-sm-9">
                    <input type="password" class="form-control" placeholder="Enter password" name="password" required>
                </div>
            </div>
            
            <div class="form-group row">
              <div class="col-lg-3 col-form-label">
                <label class="h5">Birthdate:</label>
              </div>
              <div class="col-lg-9">
                <input type="Date" class="form-control" placeholder="Enter your email" name="birthdate" required>
              </div>  
            </div>

            <div class="form-group row">
              <div class="col-sm-12 col-lg-3 col-form-label">
                <label class="h5">Gender:</label>
              </div>
              <div class="col-lg-4 col-form-label">
                <label class="form-check-label"><input type="radio" name="gender" value="Male" required><b class="h5"> Male</b></label>
              </div>
              <div class="col-lg-4 col-form-label">
                <label class="form-check-label"><input type="radio" name="gender" value="Female"><b class="h5"> Female</b></label>
              </div>                            
            </div>

            <div class="form-group row">
              <div class="col-sm-3 col-form-label">
                <label class="h5">Phone:</label>
              </div>
              <div class="col-sm-9">
                <input type="text" class="form-control" placeholder="Phone number" name="phone" required>
              </div>          
            </div>

            <div class="form-group row">
              <div class="col-sm-3 col-form-label">
                <label class="h5">Profession:</label>
              </div>
              <div class="col-sm-9 col-form-label">
                <label class="form-check-label"><input type="checkbox" name="checkDoc"><b class="h6"> Tick the checkbox if you are a doctor.</b></label>
              </div>      
            </div>

            <div class="row">
              <div class="col-sm-12">
                <div class="text-center">
                  <button class="btn btn-outline-primary btn-lg" type="submit">Sign Up</button>
                </div>
              </div>
            </div>
 
        </form>
      </div>
    </div>
  </div>
</div>


<!--Login Modal-->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Login To Your Account</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form action="login_confirm.php" method="post" class="form" role="form">

            <div class="row">
                <div class="col-sm-3 col-form-label">
                   <label class="h5">Email:</label>
                </div>
                <div class="col-sm-9">
                    <input type="email" class="form-control" placeholder="Enter your email" name="email" required>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3 col-form-label">
                   <label class="h5">Password:</label>
                </div>
                <div class="col-sm-9">
                    <input type="password" class="form-control" placeholder="Enter password" name="password" required><br>
                </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <div class="text-center">
                  <button class="btn btn-outline-primary btn-lg" type="submit">Log In</button>
                </div>
              </div>
            </div>

        </form>
      </div>
      <div class="modal-footer"> 
        <button type="button" class="btn btn-danger" style="width: 100px; background-color: red;" href="#" data-toggle="modal" data-target="#signUpModal">Register</button>
        <!-- Google Login Button -->
        <!-- <button type="button" class="btn btn-danger" style="width: 180px; background-color: red; padding-left: 10px;" onclick="window.location = '<?php //echo $loginurl;?>'">Login with Google</button> -->
      </div>
      <div class="btn-container">
        
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function checkPassword(){
    var password = document.forms["signUpForm"]["password"].value;


    var pLen = password.length;

    if (pLen<8) {
      alert("Your password should be atleast 8 characters long");

      return false;
    }
  }
</script>