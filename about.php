<?php
session_start();
if(isset($_SESSION['login']) && !empty($_SESSION['login']))
{
  $log = "Log out";
}
else
{
  include 'topnav.php';
  $log ="";
}

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/nav.css">
    <link rel="stylesheet" href="css/about.css">
    <link rel="stylesheet" href="css/style.css">

    <title>About</title>
  </head>

<body>

  <?php include 'navbar.php'; ?>

<!-- MAIN CONTENT SECTION STARTS HERE -->

<div class="image-aboutus-banner">
  <div class="container">
    <div class="row">
      <div class="col-sm-8">
          <h1 class="lg-text">About Us</h1>
          <p class="image-aboutus-para">A brief description about <strong>QUESTIENT</strong> </p>
       </div>
    </div>
  </div>
</div>
<div class="container paddingTB60">
  <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-md-12">

                 <hr>

                <h2>QUESTIENT - An online Q&A platform for doctors and patients</h2>

                <hr>

                <!-- Preview Image -->

                <!-- <img class="img-responsive" src="http://mobilebusinessinsights.com/wp-content/uploads/2016/03/IBM_MobileFirst_SXSWBlog_0321_v2.jpg" alt=""> -->


                <hr>

                <!-- Post Content -->
                <p class="lead text-justify"> <strong>QUESTIENT</strong> is a online Question & Answer platform dedicated for medical science related topics. Where doctors and patients as well as general people can sign up and ask for any health related problems. There are other people who are going to suggest or answer about the best way they know to solve the problems asked. There is upvoting options to measure the acceptability of any particular answer.</p>

                <hr>

            </div>

            <!-- Blog Sidebar Widgets Column 
            <div class="col-md-4">

                
                <div class="well bgcolor-skyblue">
                    <h4>Blog Search</h4>
                    <div class="input-group">
                        <input class="form-control" type="text">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                    </div>
                    
                 </div> -->

                <!-- Blog Categories Well -->
                <!-- <div class="well">
                    <h4>Quick Links</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                   
                </div> -->

                <!-- Side Widget Well -->
                <!--<div class="well bgcolor-skyblue">
                    <h4>Side Widget Well</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
                 </div> -->

            </div>

        </div>
</div>

<!-- MAIN CONTENT SECTION ENDS HERE -->

<!-- FOOTER SECTION STARTS HERE -->
  <?php include 'footer.php'; ?>
<!-- FOOTER SECTION ENDS HERE -->

<!-- LogIn Modal -->
<!-- <?php include 'loginForm.php'; ?> -->

<!-- SignUp Modal -->
<?php include 'signUpForm.php'; ?>
<!-- SignUp Modal end-->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/fontawesome.min.js"></script>
    <script src="js/jquery.js"></script>

  </body>
</html>