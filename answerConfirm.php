<?php
include 'server.php';
session_start();

if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{
	$answer = $_POST['answer'];
	$answer = test_input($answer);

	$questionID = $_POST['postId'];
	$questionID =test_input($questionID);
	
	$userEmail = $_SESSION['login'];

	date_default_timezone_set("Asia/Dhaka");
	$date = date("Y-m-d");
	$date = test_input($date);

	$time = date("H:i:s");
	$time = test_input($time);
}

if($db_found)
{
	$answer = quote_smart($answer, $db_handle);
	$questionID = quote_smart($questionID, $db_handle);
	$date = quote_smart($date, $db_handle);
	$time = quote_smart($time, $db_handle);

	$SQL = "INSERT INTO answer (answerBody, questionId, userEmail, answerTime, answerDate) VALUES ( $answer, $questionID, $userEmail, $time, $date)";
	$result = mysqli_query($db_handle,$SQL);
	if($result)
	{
		header('Location: index.php');
	}
}


function test_input($data)
{
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

function quote_smart($value, $handle) 
{
   if (get_magic_quotes_gpc()) {
       $value = stripslashes($value);
   }
   if (!is_numeric($value)) {
       $value = "'" . mysqli_real_escape_string($handle,$value) . "'";
   }
   return $value;
}
?>