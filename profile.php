<?php
session_start();
if(isset($_SESSION['login']) && !empty($_SESSION['login']))
{
  $log = "Log out";

  include 'server.php';
  $email = $_SESSION['login'];
  if ($db_found) 
  {
    $SQL = "SELECT * FROM user WHERE (email = $email)";
    $result = mysqli_query($db_handle, $SQL);
    $db_field = mysqli_fetch_assoc($result);

    $db_field['birthday'] = strtotime($db_field['birthday']);
    $db_field['birthday'] = date("d-M-Y l", $db_field['birthday']);

    $db_field['join_date'] = strtotime($db_field['join_date']);
    $db_field['join_date'] = date("d-M-Y l", $db_field['join_date']); //profile loding

    $query = "SELECT * FROM question";
    $result = mysqli_query($db_handle, $query);
    $total_question = mysqli_num_rows($result); // to count total question

    $query2 = "SELECT * FROM question WHERE (user_email = $email)";
    $result = mysqli_query($db_handle, $query2);
    $user_total_question = mysqli_num_rows($result); // to count total question by user

    $query3 = "SELECT * FROM answer";
    $result3 = mysqli_query($db_handle, $query3);
    $answer = mysqli_num_rows($result3);

    $query4 = "SELECT * FROM answer WHERE(userEmail = $email)";
    $result4 = mysqli_query($db_handle, $query4);
    $user_total_answer = mysqli_num_rows($result4); // to count total question by user
  }
  else 
  {
    print "User Details not found";
  }

  mysqli_close($db_handle);
}
else
{
  header('Location: index.php');
}

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/nav.css">
    <link rel="stylesheet" href="css/about.css">
    <link rel="stylesheet" href="css/profile.css">

    <title>USER Profile</title>

    <style>
        #footer {
       position:fixed;
       bottom:0;
       width:100%;
  }
    </style>
  </head>
  <body>

    <?php include 'navbar.php'; ?>

<!-- MAIN CONTENT SECTION STARTS HERE -->

<section class="mainContent">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="blogNforum">             
          <div class="panel panel-info">
            <div class="panel-body">
              <div class="row">

                  <div class="col-md-3" align="center">
                  	<?php if($db_field['gender'] == 'Male'): ?> 
                    	<?php echo '<img alt="User Pic" src="picture/male.png" class="img-fluid"> '; ?>
                    <?php else: ?>
                    	<?php echo '<img src="picture/female.png" class="img-fluid">'; ?>
                    <?php endif; ?>
                  </div>
                  
                  <div class="col-md-9">
                    <h3 class="panel-title text-center"><?= $db_field['first_name']." ".$db_field['last_name'] ?></h3> 
                    <table class="table table-user-information">
                      <tbody>
                        <tr>
                          <td>Profession</td>
                          <td><?= $db_field['profession'] ?></td>
                        </tr>
                        <tr>
                          <td>Date of Birth</td>
                          <td><?= $db_field['birthday'] ?></td>
                        </tr>
                        <tr>
                          <td>Join date</td>
                          <td><?= $db_field['join_date'] ?></td>
                        </tr>
                        <tr>
                          <td>Gender</td>
                          <td><?= $db_field['gender'] ?></td>
                        </tr>
                        <tr>
                          <td>Email</td>
                          <td><a href="#"><?= $db_field['email'] ?></a></td>
                        </tr>
                        <tr>
                          <td>Phone Number</td>
                          <td><?= "0".$db_field['phone_number'] ?></td>
                        </tr>  
                        <tr>
                          <td>Question Ask</td>
                          <td><?= $user_total_question?></td>
                        </tr>
                        <tr>
                          <td>No of Reply</td>
                          <td><?= $user_total_answer ?></td>
                        </tr>                     
                      </tbody>
                    </table>
                    <div class="text-center">
		            	<div  data-toggle="modal" data-target="#editModal">
		            		<a href="#" style="color: red;">
		            			<i class="fas fa-user-edit" style="margin-right: 5px; "></i>Edit Profile
		            		</a>
		            	</div>
            		</div>
                  </div>

              </div><!--end profile row-->
            </div> <!--panel-body-->                 
          </div><!--panel-info End -->
        </div><!--blog div end-->
      </div> <!-- col-md-8 end-->

      <div class="col-md-4">
        <div class="sideBar">
          <h1 class="text-center">STATS</h1>
          <hr class="sideBarHr">
          <div class="sideBarStatBox">
            <h3><i class="fas fa-question-circle"></i>Total Queestions (<?= $total_question?>)</h3>
          </div>
          <div class="sideBarStatBox">
            <h3><i class="fas fa-reply"></i>Total Answers (<?= $answer ?>)</h3>
          </div>
          <!-- <div class="sideBarStatBox">
            <h3><i class="fas fa-thumbs-up"></i>Likes (200)</h3>
          </div>
          <div class="sideBarStatBox">
            <h3><i class="fas fa-thumbs-down"></i>Dislikes (102)</h3>
          </div> -->
        </div>
      </div>

    </div><!--end Row-->
  </div><!--end container-->
</section>

<!-- MAIN CONTENT SECTION ENDS HERE -->


<!-- FOOTER SECTION STARTS HERE -->
<?php include 'footer.php'; ?>
<!-- FOOTER SECTION ENDS HERE -->


<!-- Edit Profile Modal -->
<?php include 'editProfile.php'; ?>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/fontawesome.min.js"></script>
    <script src="js/jquery.js"></script>

  </body>
</html>