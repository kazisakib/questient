<?php
  $email = $_SESSION['login'];
  include 'server.php';

    if ($db_found) 
    {
        $SQL = "SELECT * FROM user WHERE email = $email";
        $result = mysqli_query($db_handle, $SQL);
        $db_field = mysqli_fetch_assoc($result);
    }
    else 
    {
        print "Database NOT Found ";
    }
    mysqli_close($db_handle);
?>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Your Profile Here... </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="editProfile_confirm.php" method="post" class="form" role="form">
            <div class="row">
                <div class="col-sm-3 col-form-label">
                   <label class="h5">Name:</label>
                </div>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="firstname" placeholder="<?= $db_field['first_name'] ?>">
                </div>
                <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="<?= $db_field['last_name'] ?>" name="lastname">
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3 col-form-label">
                   <label class="h5">Email:</label>
                </div>
                <div class="col-sm-9">
                    <input type="email" class="form-control" placeholder="<?= $db_field['email'] ?>" name="email">
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3 col-form-label">
                   <label class="h5">Password:</label>
                </div>
                <div class="col-sm-9">
                    <input type="password" class="form-control" placeholder="Enter new password" name="password">
                </div>
            </div>
            
            <div class="form-group row">
              <div class="col-lg-3 col-form-label">
                <label class="h5">Birthdate:</label>
              </div>
              <div class="col-lg-9">
                <input type="Date" class="form-control" placeholder="<?= $db_field['birthday'] ?>" name="birthdate">
              </div>  
            </div>

            <div class="form-group row">
              <div class="col-sm-12 col-lg-3 col-form-label">
                <label class="h5">Gender:</label>
              </div>
              <div class="col-lg-4 col-form-label">
                <label class="form-check-label"><input type="radio" name="gender" value="Male"><b class="h5"> Male</b></label>
              </div>
              <div class="col-lg-4 col-form-label">
                <label class="form-check-label"><input type="radio" name="gender" value="Female"><b class="h5"> Female</b></label>
              </div>                            
            </div>

            <div class="form-group row">
              <div class="col-sm-3 col-form-label">
                <label class="h5">Phone:</label>
              </div>
              <div class="col-sm-9">
                <input type="text" class="form-control" placeholder="<?= '0'.$db_field['phone_number'] ?>" name="phone">
              </div>          
            </div>

            <div class="form-group row">
              <div class="col-sm-3 col-form-label">
                <label class="h5">Profession:</label>
              </div>
              <div class="col-sm-9 col-form-label">
                <label class="form-check-label"><input type="checkbox" name="checkDoc"><b class="h6"> Tick the checkbox if you are a doctor.</b></label>
              </div>      
            </div>
            <input type="hidden" name="user" value="<?= $db_field['email'] ?>" />
            <div class="row">
              <div class="col-sm-12">
                <div class="text-center">
                  <button class="btn btn-outline-primary btn-lg" type="submit">Update Profile</button>
                </div>
              </div>
            </div>
 
        </form>
      </div>
    </div>
  </div>
</div>