<div class="modal fade" id="editQuestion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Your Question</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <div class="modal-body">
        <form action="QuestionEditConfirm.php" method="post" class="form" role="form">

            <div class="row">
                <div class="col-sm-3 col-form-label">
                   <label class="h5">Title:</label>
                </div>
                <div class="col-sm-9">
                    <input id="text" name="title" class="form-control" value="<?= $db_field['post_title'] ?>">
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-form-label">
                   <label class="h5">Description of your Query:</label>
                </div>
                <div class="col-sm-12">
                    <textarea id="textarea" name="question" cols="40" rows="5" maxlength="20000" class="form-control"><?= $db_field['post_question'] ?></textarea>
                </div>
            </div>

            <input type="hidden" name="postid" value="<?= $db_field['post_id'] ?>" />
            <div class="row">
              <div class="col-sm-12">
                <div class="text-center">
                  <hr>
                  <button class="btn btn-outline-primary btn-lg" type="submit">Update Question</button>
                </div>
              </div>
            </div>
 
        </form>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="deleteQuestion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Delete Question???</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <div class="modal-body">
        <h6 class="modal-title">Are you sure, You want to delete this question??? You can edit it if you just need to change something.</h6>
        <form action="deleteQuestion.php" method="post" class="form" role="form">

            <input type="hidden" name="postid" value="<?= $db_field['post_id'] ?>" />
            <div class="row">
              <div class="col-sm-12">
                <div class="text-center">
                  <hr>
                  <button class="btn btn-outline-danger btn-lg" type="submit">Delete Question</button>
                </div>
              </div>
            </div>
 
        </form>
      </div>
    </div>
  </div>
</div>