<?php
	session_start();
	if (!isset($_SESSION['access_token']))
	{
		header(location : index.php); //for those who doesn't have access token
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<title>Login with Google</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" rel="stylesheet"/>
</head>
<body>
	<div class="container" style="margin-top: 100px;">
		<h2>Welcome <?php echo $_SESSION['givenname']; ?></h2>
		<br>
		<br>
		<div class="col-md-3">
			<img style="width: 80%;" src="<?php echo $_SESSION['picture']; ?>">
		</div>
		<div class="col-md-6">
			<table class="table table-hover table-bordered">
				<tbody>
					<tr>
						<td>First Name</td>
						<td><?php echo $_SESSION['givenname'];?></td>
					</tr>
					<tr>
						<td>Last Name</td>
						<td><?php echo $_SESSION['familyname'];?></td>
					</tr>
					<tr>
						<td>Email</td>
						<td><?php echo $_SESSION['email'];?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>