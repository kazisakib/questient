<?php
	include 'server.php';

	$fname = $lname = $email = $pword = $date = $gender = $phone = $profession = "" ;
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') 
	{
		$fname = $_POST['firstname'];
		$fname = test_input($fname);

		$lname = $_POST['lastname'];
		$lname = test_input($lname);

		$email = $_POST['email'];
		$email = test_input($email);

		$pword = $_POST['password'];
		$pword = test_input($pword);

		$date =  $_POST['birthdate'];

		$gender = $_POST['gender'];
		$gender = test_input($gender);

		$phone = "0".$_POST['phone'];
		$phone = test_input($phone);

		date_default_timezone_set("Asia/Dhaka");
		$jdate = date("Y-m-d");

		if(isset($_POST['checkDoc']) && $_POST['checkDoc']!="")
		{
    		$profession = "Doctor";
    		$profession = test_input($profession);
		}
		else
		{
			$profession = "Not Given";
		}
	}

	if ($db_found) 
	{
		$fname = quote_smart($fname, $db_handle);
		$lname = quote_smart($lname, $db_handle);
		$email = quote_smart($email, $db_handle);
		$pword = quote_smart($pword, $db_handle);
		$date = quote_smart($date, $db_handle);
		$gender = quote_smart($gender, $db_handle);
		$phone = quote_smart($phone, $db_handle);
		$profession =quote_smart($profession, $db_handle);
		$jdate = quote_smart($jdate, $db_handle);

		$SQL = "SELECT * FROM user WHERE email = $email";
		$result = mysqli_query($db_handle, $SQL);
		$num_rows = mysqli_num_rows($result);

		if ($num_rows > 0) 
		{
			echo "This Email Has already been registered.";
		}
		else
		{
			$SQL = "INSERT INTO user (first_name, last_name, email, password, birthday, gender, phone_number, profession, join_date) VALUES ($fname, $lname, $email, $pword, $date, $gender, $phone, $profession, $jdate)";
			$result = mysqli_query($db_handle,$SQL);
			//echo (boolval($result) ? 'true' : 'false');
			mysqli_close($db_handle);
		}
		if($result)
		{
			header('Location: index.php');
		}
		else
		{
			echo "<h1>Error Signing in</h1>";
		}
	}

function test_input($data)
{
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

function quote_smart($value, $handle) 
{
   if (get_magic_quotes_gpc()) {
       $value = stripslashes($value);
   }
   if (!is_numeric($value)) {
       $value = "'" . mysqli_real_escape_string($handle,$value) . "'";
   }
   return $value;
}

?>