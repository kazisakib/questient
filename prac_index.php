<?php
	include 'server.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>Practice test</title>
</head>
<body>
	<div class="tabsBgFixed">
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

  <a style="text-decoration: none; color: #000;" href="#">  
    <div class="blogPost">
      <div class="row">
        <div class="col-md-3">
          <div class="posterInfo">
            <div class="pIimage">
              <img src="picture/male.png">
            </div>
            <?php 
            	$sql = "select user.first_name, question.post_title, question.post_question, question.post_time, question.post_date FROM question INNER JOIN user on question.user_email=user.email ORDER BY post_date DESC, post_time DESC" ;
            	$res = mysqli_query($db_handle, $sql);
            	$total_question = mysqli_num_rows($res);

            	// echo $total_question;

            	if ($res->num_rows > 0):
            		$count = 1;
            		while ($db_field = $res->fetch_assoc() ):

             ?>


            <h5><?= $db_field['first_name'] ?></h5>
          </div>
        </div>
        <div class="col-md-9">
          <div class="postContent">
            <h3><?= $db_field['post_title'] ?></h3>
            <p>
              <?= $db_field['post_question'] ?>
            </p>
            <hr style="border: 1px solid #000;">
            <ul>
              <li>
                <span><i class="far fa-clock"></i><?= $db_field['post_time'] ?></span>
              </li>
          <?php endwhile; ?>
          <?php else:
          		echo"Nothing"; ?>

          <?php endif; ?>
              <li>
                <span><i class="fas fa-thumbs-up"></i>84 Likes</span>
              </li>
              <li>
                <span><i class="fas fa-thumbs-down"></i>21 Dislikes</span>
              </li>
              <li>
                <span><i class="fas fa-comment"></i>45 Comments</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </a>
  <a style="text-decoration: none; color: #000;" href="#">  
</body>
</html>