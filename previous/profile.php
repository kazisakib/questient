<?PHP
session_start();
if(!(isset($_SESSION['login']) && !empty($_SESSION['login'])))
{
	header('Location: login.php');
}
else
{
	include 'server.php';
	$email = $_SESSION['login'];
	if ($db_found) 
	{
		$SQL = "SELECT * FROM user WHERE (email = $email)";
		$result = mysqli_query($db_handle, $SQL);
		$db_field = mysqli_fetch_assoc($result);
	}
	else 
	{
		print "User Details not found";
	}

	mysqli_close($db_handle);
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>User-Profile</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.3/sketchy/bootstrap.min.css" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="alert alert-secondary text-center" role="alert">
			<h2>User Profile</h2>
		</div>
		<div class="jumbotron">
			<div class="row">
				<div class="col-sm-12 col-lg-4">
					<?php if($db_field['profession'] == 'Doctor'): ?>
						<?php echo '<img src="picture/Doctor.png" class="img-fluid">'; ?>
					<?php else: ?>
						<?php if($db_field['gender'] == 'Male'): ?>
							<?php echo '<img src="picture/mDemo.png" class="img-fluid">'; ?>
						<?php else: ?>
							<?php echo '<img src="picture/fDemo.png" class="img-fluid">'; ?>
						<?php endif; ?>
					<?php endif; ?>
				</div>
				<div class="col-sm-12 col-lg-8">
					<div class="row">
							<div class="col-sm-4 col-lg-3"><h3>Name:</h3></div>
							<div class="col-sm-8 col-lg-9"><h3><?= $db_field['first_name']." ".$db_field['last_name'] ?></h3></div>
					</div>
					<div class="row">
							<div class="col-sm-4 col-lg-3"><h3>Email:</h3></div>
							<div class="col-sm-8 col-lg-9"><h4><?= $db_field['email'] ?></h4></div>
					</div>
					<div class="row">
							<div class="col-sm-4 col-lg-3"><h3>Birthday:</h3></div>
							<div class="col-sm-8 col-lg-9"><h3><?= $db_field['birthday'] ?></h3></div>
					</div>
					<div class="row">
							<div class="col-sm-4 col-lg-3"><h3>Gender:</h3></div>
							<div class="col-sm-8 col-lg-9"><h3><?= $db_field['gender'] ?></h3></div>
					</div>
					<div class="row">
							<div class="col-sm-4 col-lg-3"><h3>Phone:</h3></div>
							<div class="col-sm-8 col-lg-9"><h3><?= "0".$db_field['phone_number'] ?></h3></div>
					</div>
					<div class="row">
						<div class="col-sm-4 col-lg-3"><h3><?= "Profession:"?></h3></div>
							<div class="col-sm-8 col-lg-9"><h3>
								<?php if($db_field['profession'] == 'Doctor'): ?>
										<?= $db_field['profession']."." ?>
								<?php else: ?>
									<?=	"Profession Not Given." ?>
								<?php endif; ?>
							</h3></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- script use for toggle -->
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"
    integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous">
 	</script>

</body>
</html>