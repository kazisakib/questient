<?php
session_start();
if(isset($_SESSION['login']) && !empty($_SESSION['login']))
{
	header('Location: home.php');
}
if(isset($_SESSION['login_error']) && !empty($_SESSION['login_error']))
{
	//echo $_SESSION['login_error'];
	//Bootstrap Modal
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Questient - Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.3/sketchy/bootstrap.min.css" crossorigin="anonymous">
	<style type="text/css">
		body{
			background-image: url("picture/bg.jpg");
		}
		.jumbotron
		{
			background-image: url("picture/bg3.jpg");
			background-color : #DCDCDC;
		}
	</style>
</head>
<body>
	<div class="container mt-4 mb-4">
		<div class="jumbotron">
			<h1 class="display-4 text-center">Login</h1><hr>

			<form method="post" action="login_confirm.php">

				<div class="row">

					<div class="col-sm-3">
				     	<img class="img-fluid" src="picture/login_img.jpg">
					</div>

					<div class="col-sm-9">
						<div class="form-group row">
							<div class="col-lg-3 col-form-label">
								<label class="h4">Email:</label>
							</div>
							<div class="col-lg-9">
								<input type="email" class="form-control" placeholder="Enter your email" name="email" required>
							</div>	
						</div>

						<div class="form-group row">
							<div class="col-lg-3 col-form-label">
								<label class="h4">Password:</label>
							</div>
							<div class="col-lg-9">
								<input type="password" class="form-control" placeholder="Enter password" name="password" required>
							</div>					
						</div>

						<div class="form-group row">
							<div class="col-lg-3 col-lg-push-9">
								<label class="h4"></label>
							</div>
							<div class="col-lg-9 col-lg-pull-3">
								<button type="submit" class="btn btn-outline-primary btn-lg">Login</button>
							</div>					
						</div>
					</div>

				</div>

			</form> <!-- Form end-->
		</div> 
	</div> <!--Form container end -->

	<div class="container mt-4">
		<div class="mt-4 mb-3">
			<hr>
    		<div class="text-muted text-center"> Alright's reserved by Questient</div>
    		<hr>
  		</div>
	</div>

	<!-- script use for toggle -->
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"
    integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous">
 	</script>

</body>
</html>