<?php

include 'server.php';
$id = $_POST['postid'];
$id = test_input($id);
$id = quote_smart($id, $db_handle);

$title = $postBody = "";

if ($_SERVER['REQUEST_METHOD'] == 'POST') 
	{
		$title = $_POST['title'];
		if($title!="")
		{
			$title = test_input($title);
			$title = quote_smart($title, $db_handle);
			$SQL = "UPDATE question SET post_title = $title WHERE post_id = $id";
			$result = mysqli_query($db_handle,$SQL);
		}

		$postBody = $_POST['question'];
		if($postBody!="")
		{
			$postBody = test_input($postBody);
			$postBody = quote_smart($postBody, $db_handle);
			$SQL = "UPDATE question SET post_question = $postBody WHERE post_id = $id";
			$result = mysqli_query($db_handle,$SQL);
		}	
	}
	header('Location: your_question.php');

function test_input($data)
{
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

function quote_smart($value, $handle) 
{
   if (get_magic_quotes_gpc()) {
       $value = stripslashes($value);
   }
   if (!is_numeric($value)) {
       $value = "'" . mysqli_real_escape_string($handle,$value) . "'";
   }
   return $value;
}

?>