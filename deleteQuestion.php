<?php

include 'server.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{
	$id = $_POST['postid'];
	$id = test_input($id);
	$id = quote_smart($id, $db_handle);
}
if($db_found)
{
	$SQL = "DELETE FROM question WHERE post_id = $id";
	$result = mysqli_query($db_handle,$SQL);
	header('Location: your_question.php');
}

function test_input($data)
{
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

function quote_smart($value, $handle) 
{
   if (get_magic_quotes_gpc()) {
       $value = stripslashes($value);
   }
   if (!is_numeric($value)) {
       $value = "'" . mysqli_real_escape_string($handle,$value) . "'";
   }
   return $value;
}

?>