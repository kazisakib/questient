<?php
include 'server.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') 
	{
		$uemail = $_POST['user'];

		$title = $_POST['title'];
		$title = test_input($title);

		$postBody = $_POST['question'];
		$postBody = test_input($postBody);

		date_default_timezone_set("Asia/Dhaka");
		$postdate = date("Y-m-d");
		$postdate = test_input($postdate);

		$postTime = date("H:i:s");
		$postTime = test_input($postTime);
	}

	if ($db_found) 
	{
		$title = quote_smart($title, $db_handle);
		$postBody = quote_smart($postBody, $db_handle);
		$postdate = quote_smart($postdate, $db_handle);
		$postTime = quote_smart($postTime, $db_handle);

		$SQL = "INSERT INTO question (post_title, post_question, user_email, post_time, post_date) VALUES ($title, $postBody, $uemail, $postTime, $postdate)";
		$result = mysqli_query($db_handle,$SQL);
		mysqli_close($db_handle);
		
		if($result)
		{
			header('Location: your_question.php');
		}
		else
		{
			echo "<h1>Error Posting Your Query</h1>";
		}
	}

function test_input($data)
{
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

function quote_smart($value, $handle) 
{
   if (get_magic_quotes_gpc()) {
       $value = stripslashes($value);
   }
   if (!is_numeric($value)) {
       $value = "'" . mysqli_real_escape_string($handle,$value) . "'";
   }
   return $value;
}

?>