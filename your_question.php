<?php
	session_start();
	if(isset($_SESSION['login']) && !empty($_SESSION['login']))
	{
		  $log = "Log out";
		  date_default_timezone_set("Asia/Dhaka");
		  include 'server.php';
		  $email = $_SESSION['login'];
		  if ($db_found) 
		  {
			    $SQL = "SELECT * FROM question WHERE (user_email = $email)";
			    $result = mysqli_query($db_handle, $SQL);
		  }
		  else 
		  {
		    	print "User Details not found";
		  }
		  mysqli_close($db_handle);
	}
	else
	{
	  	header('Location: index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/nav.css">
    <link rel="stylesheet" href="css/about.css">
	<title>Your Question's</title>
	<style>
	#footer {
		   position:fixed;
		   bottom:0;
		   width:100%;
	}
	</style>
</head>
<body>
	<!--navbar starts here  -->
    <?php include 'navbar.php'; ?>
    <!--navbar ends here  -->

    <div class="alert alert-primary">
      	<div class="display-4 text-center">All OF Your Question's</div> 
    </div>

	<div class="container">
		<div class="row table-responsive">
			<div class="col-sm-12" style="height: 500px; width: 100%; overflow-y: scroll;">
				<table class="table table-striped table-dark table-hover" style="margin-left: 10px;">
					<thead style="color: red;" class="font-weight-bold h6">
						<tr>
							<th scope="col">Serial No.</th>
							<th scope="col">Title</th>
							<th scope="col">Post Time</th>
							<th scope="col">Post Date</th>
						</tr>
					</thead>
					<tbody>
						<?php if($result->num_rows > 0):
							$count = 1;
							while($db_field = $result->fetch_assoc()  ): ?>
						<tr>
							<th scope="row"><?= $count++ ?></th>
							<td>
		            			<a href="post.php?PID=<?php echo $db_field['post_id']; ?>" style="color:red;"><?= $db_field['post_title'] ?></a>
							</td>
							<td><?= $db_field['post_time'] ?></td>
							<td><?= $db_field['post_date'] ?></td>
						</tr>
						<?php endwhile; ?>
						<?php else: ?>
								<td class="h2" colspan="6">
									No Question found.
								</td>
								<?php endif; ?>
					</tbody>
				</table><br><br>
			</div>
		</div>
	</div>
	<?php 
		include 'footer.php';
	?>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>
</html>