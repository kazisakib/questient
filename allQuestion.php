<?php
session_start();
include 'server.php';
if(isset($_SESSION['login']) && !empty($_SESSION['login']))
{
  $log = "Log out";
}
else
{
  include 'topnav.php';
  $log =NULL;
}
    $SQL = "SELECT * FROM user";                // for sidebar
    $result = mysqli_query($db_handle, $SQL);
    $user = mysqli_num_rows($result);

    $SQL_3 = "SELECT * FROM question";
    $result3 = mysqli_query($db_handle, $SQL_3);
    $total_question = mysqli_num_rows($result3);

    $SQL = "SELECT * FROM answer";
    $result = mysqli_query($db_handle, $SQL);
    $no_of_answer = mysqli_num_rows($result);

    $question = array();  
    $answer = array();

    if($result3->num_rows > 0)
    {
      while($db_field = $result3->fetch_assoc())
      {
        $qID = $db_field['post_id'];
        $SQL = "SELECT * FROM answer WHERE (questionId = $qID)";
        $result = mysqli_query($db_handle, $SQL); 
        $total_answer = mysqli_num_rows($result);

        $question[] = $qID;
        $answer[] = $total_answer;
      }
    }

    for($i=0; $i<$total_question; $i++)
    {
      for ($j=$i+1; $j < $total_question; $j++) 
      { 
        if ($answer[$i]>$answer[$j]) 
        {
          $maxquestion = $question[$i];
          $question[$i] = $question[$j];
          $question[$j] = $maxquestion;

          $maxanswer = $answer[$i];
          $answer[$i] = $answer[$j];
          $answer[$j] = $maxanswer;
        }
      }
    }

function quote_smart($value, $handle) 
{
   if (get_magic_quotes_gpc()) {
       $value = stripslashes($value);
   }
   if (!is_numeric($value)) {
       $value = "'" . mysqli_real_escape_string($handle,$value) . "'";
   }
   return $value;
}
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" rel="stylesheet"/>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css"/>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/nav.css">
    <link rel="stylesheet" href="css/about.css">
  <title>All Question</title>

  <style>
      #footer {
           position:fixed;
           bottom:0;
           width:100%;
      }
  </style>
</head>
<body>

  <?php
    include 'navbar.php';
    include 'signUpForm.php';
  ?>


<section class="mainContent">
  <div class="container">
    <div class="row">

      <div class="col-md-8">
        <h2>All Questions</h2> <hr>
        <ul class="nav nav-pills nav-stacked">
          <li class="active"><a data-toggle="pill" href="#home">Most Recents</a></li>
          <li><a data-toggle="pill" href="#menu1">Top Trending</a></li>
          <li><a data-toggle="pill" href="#menu2">Unanswered</a></li>
        </ul>
        <hr><hr>

          <div class="tab-content" style="height: 300px; overflow-y: scroll;">
            
            <div id="home" class="tab-pane fade in active"> <!--Most Answered-->
              <?php 
                  $sql = "select user.first_name, user.gender, question.post_id, question.post_title, question.post_question, question.post_time, question.post_date FROM question INNER JOIN user on question.user_email=user.email ORDER BY post_date DESC, post_time DESC" ;
                    $res = mysqli_query($db_handle, $sql);

                    $record = array();

                    while($row = $res->fetch_assoc()) 
                    {
                      $record[] = $row;
                    }       
              ?>
              <?php foreach($record as $rec){?>
              <div class="well">
                  <div class="media">

                    <div class="pull-left">
                      <?php if($rec['gender'] == 'Male'): ?> 
                              <?php echo '<img alt="User Pic" src="picture/male.png" class="img-fluid" height="100" width="100"> '; ?>
                      <?php else: ?>
                              <?php echo '<img src="picture/female.png" class="img-fluid" height="100" width="100"> '; ?>
                      <?php endif; ?>                    
                    </div>

                    <div class="media-body">
                      <?php $qid = $rec['post_id']; ?>
                      <a href="question.php?PID=<?php echo $qid ?>"><h4 class="media-heading"><?php echo $rec['post_title'] ?></h4></a>                      
                      <p class="text-right">By <?php echo $rec['first_name'] ?></p>
                      <p style="width: 420px"><?php echo $rec['post_question'] ?></p>
                      <ul class="list-inline list-unstyled">
                          <li><span><i class="glyphicon glyphicon-calendar"></i> <?php echo $rec['post_date'] ?> </span></li>
                              <li>|</li>
                              <?php 
                                  $Time = date("h : m : s a",strtotime($rec['post_time'])); ?>
                              <li><span><i class="class= far fa-clock"></i> <?php echo $Time; ?> </span></li>
                              <li>|</li>
                              <?php
                                  $SQL_3 = "SELECT * FROM answer WHERE (questionId = $qid)";   
                                  $result3 = mysqli_query($db_handle, $SQL_3);
                                  $user_total_answer = mysqli_num_rows($result3);
                              ?>
                              <li><span><i class="glyphicon glyphicon-comment"></i> <?= $user_total_answer ?> comments</span></li>                          
                      </ul>
                    </div>
                </div>
              </div>
            <?php } ?>
          
          
            </div> <!--end Most answered-->

            <div id="menu1" class="tab-pane fade"> <!--Most Treanding-->
              <?php
                  for ($i=$total_question-1; $i >= 0; $i--) 
                  { 
                    if($answer[$i]!=0)
                    {
                      $quesID = $question[$i];
                      $sql1 = "SELECT * from question WHERE (post_id = $quesID)";
                      $ress = mysqli_query($db_handle, $sql1);

                      while($row = $ress->fetch_assoc()) 
                      {
                        $chkEm = $row['user_email'];
                        $chkEm = quote_smart($chkEm, $db_handle);

                        $sql5 = "SELECT first_name,gender from user WHERE (email = $chkEm)";
                        $res5 = mysqli_query($db_handle, $sql5);
                        $db_field55 = mysqli_fetch_assoc($res5);
              ?>
                    <div class="well">
                      <div class="media">
                        <div class="pull-left">
                            <?php if($db_field55['gender'] == 'Male'): ?> 
                                    <?php echo '<img alt="User Pic" src="picture/male.png" class="img-fluid" height="100" width="100"> '; ?>
                            <?php else: ?>
                                    <?php echo '<img src="picture/female.png" class="img-fluid" height="100" width="100"> '; ?>
                            <?php endif; ?>
                        </div>
                        <div class="media-body">
                          <?php $qid = $row['post_id']; ?>
                          <a href="question.php?PID=<?php echo $qid ?>"><h4 class="media-heading"><?php echo $row['post_title'] ?></h4> </a>
                          
                            <p class="text-right">By <?php echo $db_field55['first_name'] ?></p>
                            <p style="width: 420px"><?php echo $row['post_question'] ?></p>
                            <ul class="list-inline list-unstyled">
                              <li>
                                <span><i class="glyphicon glyphicon-calendar"></i> <?php echo $row['post_date'] ?> </span>
                              </li>
                              <li>|</li>
                                  <?php 
                                      $Time = date("h : m : s a",strtotime($row['post_time'])); 
                                  ?>
                              <li>
                                <span><i class="class= far fa-clock"></i> <?php echo $Time; ?> </span>
                              </li> 
                              <li>|</li>
                                  <?php
                                      $SQL_3 = "SELECT * FROM answer WHERE (questionId = $qid)";   
                                      $result3 = mysqli_query($db_handle, $SQL_3);
                                      $user_total_answer = mysqli_num_rows($result3);
                                  ?>
                              <li>
                                <span><i class="glyphicon glyphicon-comment"></i> <?= $user_total_answer ?> comments</span>
                              </li>                
                            </ul>
                        </div>
                      </div>
                    </div>
              <?php
                    }}}  
              ?>
          
            </div> <!--end most Treanding-->

            <div id="menu2" class="tab-pane fade"> <!--Unanswered-->
              <?php
                  for ($i=0; $i < $total_question; $i++) 
                  { 
                    if($answer[$i]==0)
                    {
                      $quesID = $question[$i];
                      $sql1 = "SELECT * from question WHERE (post_id = $quesID)";
                      $ress = mysqli_query($db_handle, $sql1);

                      while($row = $ress->fetch_assoc()) 
                      {
                        $chkEm = $row['user_email'];
                        $chkEm = quote_smart($chkEm, $db_handle);

                        $sql5 = "SELECT first_name,gender from user WHERE (email = $chkEm)";
                        $res5 = mysqli_query($db_handle, $sql5);
                        $db_field55 = mysqli_fetch_assoc($res5);
              ?>
                    <div class="well">
                      <div class="media">
                        <div class="pull-left">
                            <?php if($db_field55['gender'] == 'Male'): ?> 
                                    <?php echo '<img alt="User Pic" src="picture/male.png" class="img-fluid" height="100" width="100"> '; ?>
                            <?php else: ?>
                                    <?php echo '<img src="picture/female.png" class="img-fluid" height="100" width="100"> '; ?>
                            <?php endif; ?>
                        </div>
                        <div class="media-body">
                          <?php $qid = $row['post_id']; ?>
                          <a href="question.php?PID=<?php echo $qid ?>"><h4 class="media-heading"><?php echo $row['post_title'] ?></h4> </a>
                          
                            <p class="text-right">By <?php echo $db_field55['first_name'] ?></p>
                            <p style="width: 420px"><?php echo $row['post_question'] ?></p>
                            <ul class="list-inline list-unstyled">
                              <li>
                                <span><i class="glyphicon glyphicon-calendar"></i> <?php echo $row['post_date'] ?> </span>
                              </li>
                              <li>|</li>
                                  <?php 
                                      $Time = date("h : m : s a",strtotime($row['post_time'])); 
                                  ?>
                              <li>
                                <span><i class="class= far fa-clock"></i> <?php echo $Time; ?> </span>
                              </li> 
                              <li>|</li>
                                  <?php
                                      $SQL_3 = "SELECT * FROM answer WHERE (questionId = $qid)";   
                                      $result3 = mysqli_query($db_handle, $SQL_3);
                                      $user_total_answer = mysqli_num_rows($result3);
                                  ?>
                              <li>
                                <span><i class="glyphicon glyphicon-comment"></i> <?= $user_total_answer ?> comments</span>
                              </li>                
                            </ul>
                        </div>
                      </div>
                    </div>
              <?php
                    }}}  
              ?>
            </div> <!--end Unanswered-->
          </div>
      </div><!--end col-md-8-->


      <div class="col-md-4">
        <div class="sideBar">
          <h1>STATS</h1>
          <hr class="sideBarHr">
          <div class="sideBarStatBox">
            <h3><i class="fas fa-users"></i>USERS (<?= $user ?>)</h3>
          </div>
          <div class="sideBarStatBox">
            <h3><i class="fas fa-question"></i>Questions (<?= $total_question?>)</h3>
          </div>
          <div class="sideBarStatBox">
            <h3><i class="fas fa-comments"></i>Total Answers (<?= $no_of_answer ?>)</h3>
          </div>
          <!-- <div class="sideBarStatBox">
            <h3><i class="fas fa-book-reader"></i>Unsolved (345)</h3>
          </div> -->
        </div>
      </div>

    </div>
  </div>
</section>

<!-- MAIN CONTENT SECTION ENDS HERE -->

  <?php include 'footer.php'; ?>

  <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/fontawesome.min.js"></script>
    <script src="js/jquery.js"></script>

</body>
</html>