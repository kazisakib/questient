<?php
	require_once "googleConfig.php";

	if(isset($_SESSION['access_token']))
	{
		$gclient->setAccessToken($_SESSION['access_token']); //For anyone who already logged in

	}

	else if(isset($_GET['code']))
	{
		$token = $gclient->fetchAccessTokenWithAuthCode($_GET['code']);
		$_SESSION['access_token'] = $token; //for authorized people
	}
	else{
		header("location : index.php"); //for those who are not authorized
	}

	$oAuth = new Google_Service_Oauth2($gclient);
	$userdata = $oAuth->userinfo_v2_me->get();

	//data we get from Google+ profile
	$_SESSION['email'] = $userdata['email'];
	$_SESSION['picture'] = $userdata['picture'];
	$_SESSION['givenname'] = $userdata['givennName'];
	$_SESSION['familyname'] = $userdata['familyName'];

	header("location : welcome.php");
	exit();

?>