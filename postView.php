<?php
session_start();
if(isset($_SESSION['login']) && !empty($_SESSION['login']))
{
    $log = "Log out";

    include 'server.php';
    if (isset($_GET['PID'])) 
    {
        $postID =  $_GET['PID'];
    }
    if ($db_found) 
    {
        $SQL = "SELECT * FROM question WHERE (post_id = $postID)";
        $result = mysqli_query($db_handle, $SQL);
        //echo (boolval($result) ? 'true' : 'false');
        $db_field = mysqli_fetch_assoc($result);
        $email = $_SESSION['login'];
        $SQL_2 = "SELECT first_name, last_name FROM user WHERE (email = $email)";
        $result2 = mysqli_query($db_handle, $SQL_2);
        //echo (boolval($result2) ? 'true' : 'false');
        $db_field2 = mysqli_fetch_assoc($result2);
    }
        mysqli_close($db_handle);
    }
    else
    {
      header('Location: index.php');
    }
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/nav.css">
    <link rel="stylesheet" href="css/about.css">
    <link rel="stylesheet" href="css/profile.css">

    <title>Post View</title>
  </head>
  <body>


  <?php include "navbar.php"; ?>
  <?php include "editquestion.php"; ?>

<!-- MAIN CONTENT SECTION STARTS HERE -->

<section class="mainContent">
  <div class="container">
    <div class="col-sm-8">
        <div class="panel panel-white post panel-shadow">
            <div class="post-heading row">

                <div class="col-sm-3 col-md-2">
                    <img src="http://bootdey.com/img/Content/user_1.jpg" class="img-fluid" alt="user profile image">
                </div>

                <div class="col-sm-9 col-md-10">
                    <div class="title h5">
                        <a href="profile.php"><b><?= $db_field2['first_name']." ".$db_field2['last_name'] ?></b></a>
                        <br>
                        <h6 class=""><?= $db_field['post_date']." ".$db_field['post_time'] ?></h6>
                        <div  data-toggle="modal" data-target="#editQuestion">
                            <button class="btn btn-outline-dark btn-sm">Edit</button>
                        </div>
                    </div>
                </div>

                

            </div> 
            <br>
            <div class="post-description"> 
                <p><?= $db_field['post_question'] ?></p>
                <div class="stats">
                    <a href="#" class="btn btn-default stat-item">
                        <i class="fa fa-thumbs-up icon"></i>2
                    </a>
                    <a href="#" class="btn btn-default stat-item">
                        <i class="fa fa-share icon"></i>12
                    </a>
                </div>
            </div>



            <br><br><br><br><br><br><br><br><br><br><br>
            <div class="post-footer">
                <div class="input-group"> 
                    <input class="form-control" placeholder="Add a comment" type="text">
                    <span class="input-group-addon">
                        <a href="#"><i class="fa fa-edit"></i></a>  
                    </span>
                </div>
                <ul class="comments-list">
                    <li class="comment">
                        <a class="pull-left" href="#">
                            <img class="avatar" src="http://bootdey.com/img/Content/user_1.jpg" alt="avatar">
                        </a>
                        <div class="comment-body">
                            <div class="comment-heading">
                                <h4 class="user">Gavino Free</h4>
                                <h5 class="time">5 minutes ago</h5>
                            </div>
                            <p>Sure, oooooooooooooooohhhhhhhhhhhhhhhh</p>
                        </div>
                        <ul class="comments-list">
                            <li class="comment">
                                <a class="pull-left" href="#">
                                    <img class="avatar" src="http://bootdey.com/img/Content/user_3.jpg" alt="avatar">
                                </a>
                                <div class="comment-body">
                                    <div class="comment-heading">
                                        <h4 class="user">Ryan Haywood</h4>
                                        <h5 class="time">3 minutes ago</h5>
                                    </div>
                                    <p>Relax my friend</p>
                                </div>
                            </li> 
                            <li class="comment">
                                <a class="pull-left" href="#">
                                    <img class="avatar" src="http://bootdey.com/img/Content/user_2.jpg" alt="avatar">
                                </a>
                                <div class="comment-body">
                                    <div class="comment-heading">
                                        <h4 class="user">Gavino Free</h4>
                                        <h5 class="time">3 minutes ago</h5>
                                    </div>
                                    <p>Ok, cool.</p>
                                </div>
                            </li> 
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

</section>


<!-- MAIN CONTENT SECTION ENDS HERE -->




<!-- FOOTER SECTION STARTS HERE -->

<section class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="footerTxt">
          <h3>© QUESTIENT || 2018</h3>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- FOOTER SECTION ENDS HERE -->




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/fontawesome.min.js"></script>
    <script src="js/jquery.js"></script>

  </body>
</html>