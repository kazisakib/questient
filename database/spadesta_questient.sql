-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 17, 2018 at 11:00 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spadesta_questient`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `id` int(255) NOT NULL,
  `answerBody` varchar(20000) NOT NULL,
  `questionId` int(255) NOT NULL,
  `userEmail` varchar(50) NOT NULL,
  `answerTime` time NOT NULL,
  `answerDate` date NOT NULL,
  `likeGiven` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`id`, `answerBody`, `questionId`, `userEmail`, `answerTime`, `answerDate`, `likeGiven`) VALUES
(1, 'dlkfgnl.gh.fcgh', 8, 'tabidhasan003@gmail.com', '01:44:13', '2018-12-02', NULL),
(3, 'xdfgmlxd;fgjl xdfjg;xfmg', 1, 'tabidhasan003@gmail.com', '01:44:45', '2018-12-02', NULL),
(4, 'xdf;lgmxlfkgml;mxfg;dlfgmlxfdg', 1, 'tabidhasan003@gmail.com', '01:45:51', '2018-12-02', NULL),
(5, 'mlsfksdnfgkldgjlkfdxmg', 8, 'tabidhasan003@gmail.com', '15:50:27', '2018-12-02', NULL),
(6, 'lfdgjnmdflgh;f', 8, 'nafisa016@outlook.com', '01:44:10', '2018-12-01', NULL),
(8, 'lkgmlxdf;gjml;xdfmg', 8, 'tabidhasan003@gmail.com', '18:43:37', '2018-12-02', NULL),
(9, 'All BACONS are not HARAM Bacon is a special way of cutting down the meat and it can be prepared from different meat such as Beef, Chicken, Buffalo and Lamb etc. We only use beef and chicken Bacon for our Burgers.', 8, 'tabidhasan003@gmail.com', '19:01:20', '2018-12-02', NULL),
(10, 'The numbers of Mainland Chinese visiting Hong Kong have risen considerably\r\nover this time period. In 2002 there were nearly 7 million Mainland Chinese visitors,\r\nsimilar to the figure for other parts of Asia and just over double that for non-Asian\r\ntravelers. This number climbed to approximately 8 million in 2003, then rose\r\ndramatically by 4 million to just over 12 million only one year later. Since 2004 there\r\nhas been a steady increase, with around 18 million Mainland Chinese travelers visiting\r\nHong Kong in 2009. This is more than double the number for that year of other\r\nvisitors from Asia, and around four times the figures for non-Asian visitors.', 8, 'tabidhasan003@gmail.com', '19:01:44', '2018-12-02', NULL),
(11, 'The ORDER BY keyword is used to sort the result-set in ascending or descending order.\r\n\r\nThe ORDER BY keyword sorts the records in ascending order by default. To sort the records in descending order, use the DESC keyword.', 8, 'h.ferdous@outlook.com', '01:14:13', '2018-12-03', NULL),
(16, 'use acnofight facewash. I really works', 11, 'sakibahmad24@gmail.com', '11:56:46', '2018-12-17', NULL),
(17, 'Dr Shahara Begum Sugested that always take insulin for control,, but nort too much dose please.', 10, 'mailtosadman@gmail.com', '12:24:39', '2018-12-17', NULL),
(18, 'paracetamol 4 bela :)', 13, 'mailtosadman@gmail.com', '12:25:44', '2018-12-17', NULL),
(19, 'nice thoughts', 7, 'mailtosadman@gmail.com', '12:26:31', '2018-12-17', NULL),
(20, 'wow', 4, 'mailtosadman@gmail.com', '12:26:58', '2018-12-17', NULL),
(21, 'hmmm', 3, 'mailtosadman@gmail.com', '12:27:25', '2018-12-17', NULL),
(22, 'ok', 2, 'mailtosadman@gmail.com', '12:27:30', '2018-12-17', NULL),
(23, 'I recommended Oxy face wash for this.', 11, 'mailtosadman@gmail.com', '12:28:26', '2018-12-17', NULL),
(24, 'use fair and lovely..', 11, 'shakilur.nsu@gmail.com', '12:57:56', '2018-12-17', NULL),
(25, 'Immediately consult with a bone specialist', 14, 'sakibahmad24@gmail.com', '13:14:44', '2018-12-17', NULL),
(26, 'consult with a bone specialist', 15, 'sakibahmad24@gmail.com', '13:15:42', '2018-12-17', NULL),
(27, '3din er por Antibiotics neya uchit by age Dr dekhayen hbe.', 13, 'momu@gmail.com', '13:16:01', '2018-12-17', NULL),
(28, 'U should consult a doctor immediately.', 12, 'momu@gmail.com', '13:17:36', '2018-12-17', NULL),
(29, 'I think u have oily skin.so u should avoid all kinds of oxydise skin products.and use icecube with soft towel.', 11, 'momu@gmail.com', '13:19:52', '2018-12-17', NULL),
(30, 'ðŸ™„ðŸ™„ðŸ™„ðŸ™„', 9, 'momu@gmail.com', '13:21:16', '2018-12-17', NULL),
(31, 'use facewash', 17, 'tabidhasan003@gmail.com', '14:15:00', '2018-12-17', NULL),
(32, 'Turza boro chapabaj rply Dao na kn', 19, 'puja.ghosh@gmail.com', '14:41:45', '2018-12-17', NULL),
(33, 'chulkais na -_-', 18, 'shakilur.nsu@gmail.com', '19:36:48', '2018-12-17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `post_id` int(11) NOT NULL,
  `post_title` varchar(255) NOT NULL,
  `post_question` varchar(20000) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `post_time` time NOT NULL,
  `post_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`post_id`, `post_title`, `post_question`, `user_email`, `post_time`, `post_date`) VALUES
(1, 'My First Question', 'If the time you got back from the code is not the right time, it\'s probably because your server is in another country or set up for a different timezone.\r\n\r\nSo, if you need the time to be correct according to a specific location, you can set a timezone to use.\r\n\r\nThe example below sets the timezone to &quot;America/New_York&quot;, then outputs the current time in the specified format:', 'tabidhasan003@gmail.com', '21:53:42', '2018-11-20'),
(2, 'My Second Question', 'Anthony Edward Stark is a character portrayed by Robert Downey Jr. in the Marvel Cinematic Universe film franchise, based on the Marvel Comics character of the same name and known commonly by his alter ego, Iron Man.', 'tabidhasan003@gmail.com', '00:20:19', '2018-11-20'),
(3, 'My Third Question', 'All BACONS are not HARAM\r\n\r\nBacon is a special way of cutting down the meat and it can be prepared from different meat such as Beef, Chicken, Buffalo and Lamb etc. We only use beef and chicken Bacon for our Burgers.', 'tabidhasan003@gmail.com', '01:47:48', '2018-11-20'),
(4, 'My Forth Question', 'An article (with the linguistic glossing abbreviation ART) is a word that is used with a noun (as a standalone word or a prefix or suffix) to specify grammatical definiteness of the noun, and in some languages extending to volume or numerical scope.', 'tabidhasan003@gmail.com', '01:55:28', '2018-11-20'),
(7, 'About today', '&quot;One Day&quot; - Barbra recorded three versions of &quot;One Day&quot; in the studio. Peter Matz and Michel Legrand each tried to arrange it. The last version in May 1969 was arranged by David Shire. The song never made it onto a Streisand album. However, it was used in 1990\'s Earth Day Special.', 'nafisa016@outlook.com', '01:40:00', '2018-11-30'),
(8, 'My Fifth Question', 'All BACONS are not HARAM Bacon is a special way of cutting down the meat and it can be prepared from different meat such as Beef, Chicken, Buffalo and Lamb etc. We only use beef and chicken Bacon for our Burgers.', 'tabidhasan003@gmail.com', '16:47:01', '2018-11-30'),
(9, 'Jor hoyeche', 'jor hoynai...sorry!\r\nThank u :)', 'sakibahmad24@gmail.com', '11:47:46', '2018-12-05'),
(10, 'Diabetes type 2', 'Will a person with Type 2 diabetes under control end up with the need for insulin?', 'rahmanmarjan11@gmail.com', '11:20:40', '2018-12-17'),
(11, 'pimple problem', 'i have severe pimple problem what should i do?', 'sinthiya.islam@northsouth.edu', '11:53:04', '2018-12-17'),
(12, 'Ghar Betha', 'Ghare achomka onek betha kortese,, Lal hoye gese purai. Ki kora jay,, Any doctors suggestion?', 'mailtosadman@gmail.com', '12:20:47', '2018-12-17'),
(13, 'à¦•à§‡à¦ªà§‡ à¦•à§‡à¦ªà§‡ à¦œà§à¦¬à¦°', 'à¦†à¦®à¦¾à¦° à¦—à¦¤ à§ªà¦¦à¦¿à¦¨ à¦¯à¦¾à¦¬à¦¤ à¦•à§‡à¦ªà§‡à¦•à§‡à¦ªà§‡ à¦œà§à¦¬à¦° à¦‰à¦ à¦›à§‡, à¦•à¦¿ à¦•à¦°à¦¾à¦¯à¦¾à§Ÿ? à¦•à¦¿ à¦”à¦·à¦§ à¦–à¦¾à¦“à§Ÿà¦¾ à¦¯à¦¾à§Ÿ?', 'sakibahmad24@gmail.com', '12:22:36', '2018-12-17'),
(14, 'pain in backbone.', 'when i work long sitting somewhere continuously i feel like pain in my backbone. :(', 'shakilur.nsu@gmail.com', '13:02:54', '2018-12-17'),
(15, 'Muscle pain', 'Prai e payer Muscle e prochondo pain hoi.ki korbo??', 'momu@gmail.com', '13:14:24', '2018-12-17'),
(16, 'headache', 'headache from last 2day', 'tabidhasan003@gmail.com', '13:56:31', '2018-12-17'),
(17, 'Dark spot', 'I have some dark spot on my face . How to get rid of dark spots ?', 'samiyarahman3464@gmail.com', '14:06:46', '2018-12-17'),
(18, 'Asthma', 'What medications are best for the treatment of asthma? What are their side effects?', 'sakib_6@yahoo.com', '14:27:47', '2018-12-17'),
(20, 'Tonsil stone', 'I have got tonsil stone what should i do ?', 'rayhan.rakib@northsouth.edu', '14:58:41', '2018-12-17');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `birthday` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `profession` varchar(20) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `join_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `first_name`, `last_name`, `email`, `password`, `birthday`, `gender`, `phone_number`, `profession`, `photo`, `join_date`) VALUES
(9, 'Nafisa', 'Tonu', 'nafisa016@outlook.com', '2205', '1993-04-16', 'Female', '1793256338', 'Not Given', NULL, '2018-11-07'),
(15, 'Habiba', 'Ferdous', 'h.ferdous@outlook.com', '123', '2018-11-09', 'Female', '1537313798', 'Doctor', NULL, '2018-11-05'),
(22, 'Abid', 'Turzo', 'tabidhasan003@gmail.com', '2208', '1994-07-06', 'Male', '1923673254', 'Not Given', NULL, '2018-11-19'),
(24, 'hasan', 'Turzo', 'hasanturzo8@gmail.com', '147', '2018-11-07', 'Male', '019236732s54', 'Not Given', NULL, '2018-11-21'),
(25, 'Kazi Sakib', 'Ahmad', 'sakibahmad24@gmail.com', '26031971', '2018-12-04', 'Male', '1676451865', 'Not Given', NULL, '2018-12-05'),
(26, 'Puja', 'Ghosh', 'puja.ghosh1@gmail.com', '123sp12345', '1995-01-31', 'Female', '12345778901', 'Not Given', NULL, '2018-12-17'),
(27, 'Mijanur Rahman', 'Marjan', 'rahmanmarjan11@gmail.com', 'student102337', '1994-08-18', 'Male', '1715644450', 'Not Given', NULL, '2018-12-17'),
(28, 'Sinthiya', 'Islam', 'sinthiya.islam@northsouth.edu', '12341234', '2018-08-14', 'Female', '1748036067', 'Not Given', NULL, '2018-12-17'),
(29, 'Syed', 'Sadman Sakib', 'mailtosadman@gmail.com', 'sadmansakib56', '1994-03-12', 'Male', '1913983834', 'Not Given', NULL, '2018-12-17'),
(30, 'Shakilur', 'Rahman', 'shakilur.nsu@gmail.com', '12345678', '1993-02-19', 'Male', '101010101010', 'Not Given', NULL, '2018-12-17'),
(31, 'Mehanaz', 'Momu', 'mailtomehanaz@gmail.cim', 'momu23456', '1993-11-13', 'Female', '1689402529', 'Not Given', NULL, '2018-12-17'),
(33, 'Mehenaz', 'Momu', 'momu@gmail.com', '12345678', '2017-06-12', 'Female', '17889999789', 'Not Given', NULL, '2018-12-17'),
(34, 'Samia', 'Rahman', 'samiyarahman3464@gmail.com', '1234567@@', '2001-04-18', 'Female', '1830296708', 'Not Given', NULL, '2018-12-17'),
(35, 'Nazmus', 'Sakib', 'sakib_6@yahoo.com', '12345678', '1995-09-06', 'Male', '1676303995', 'Not Given', NULL, '2018-12-17'),
(36, 'Puja', 'Ghosh', 'puja.ghosh@gmail.com', 'shawonpuja', '1995-01-31', 'Female', '1704098921', 'Not Given', NULL, '2018-12-17'),
(37, 'Ahmed', 'Rayhan', 'rayhan.rakib@northsouth.edu', '123456789', '1995-11-13', 'Male', '1717272999', 'Not Given', NULL, '2018-12-17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answer`
--
ALTER TABLE `answer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`),
  ADD UNIQUE KEY `phone_number` (`phone_number`),
  ADD UNIQUE KEY `ID_2` (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answer`
--
ALTER TABLE `answer`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
