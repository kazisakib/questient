<?php
session_start();
include 'server.php';
if(isset($_SESSION['login']) && !empty($_SESSION['login']))
{
  $log = "Log out";
  $user = $_SESSION['login'];

    $query = "SELECT * FROM question";
    $result = mysqli_query($db_handle, $query);
    $total_question = mysqli_num_rows($result); // to count total question

    $query3 = "SELECT * FROM answer";
    $result3 = mysqli_query($db_handle, $query3);
    $answer = mysqli_num_rows($result3);

    $query2 = "SELECT * FROM question WHERE (user_email = $user)";
    $result4 = mysqli_query($db_handle, $query2);
    $user_total_question = mysqli_num_rows($result4); // to count total question by user
}
else
{
  header('Location: index.php');
}

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/nav.css">
    <link rel="stylesheet" href="css/about.css">    
    <link rel="stylesheet"  href="css/style.css">

    <title>Ask Question</title>

    <style>
      #footer {
           position:fixed;
           bottom:0;
           width:100%;
      }
  </style>
  </head>
  <body>

  <?php include 'navbar.php'; ?>

<!-- MAIN CONTENT SECTION STARTS HERE -->

<section class="mainContent">
  <div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                
                <div class="row">
                    <div class="col-md-8">
                      <div class="row">
                          <div class="col-sm-12">
                              <h4 class="text-center">Ask a Question</h4>
                          </div>                    
                      </div>
                      <hr>
                        <form method="post" action="addPost_confirm.php">
                          <input type="hidden" name="user" value="<?= $user ?>" />
                              <div class="form-group row">
                                <label for="text" class="col-12 col-form-label">Enter The Title here</label> 
                                <div class="col-12">
                                  <input id="text" name="title" placeholder="Enter Title here" class="form-control here" required="required" type="text">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="textarea" class="col-12 col-form-label">Description of your Query</label> 
                                <div class="col-12">
                                  <textarea id="textarea" name="question" cols="40" rows="5" class="form-control" required></textarea>
                                </div>
                              </div> 
                              <div class="row">
                                <div class="col-sm-12">
                                  <div class="text-center">
                                    <button class="btn btn-outline-primary btn-lg" type="submit">Ask Now</button>
                                  </div>
                                </div>
                              </div>
                            </form>
                    </div>
                    <div class="col-md-4 ">
                      <div class="sideBar">
                        <h1 class="text-center">STATS</h1>
                        <hr class="sideBarHr">
                        <div class="sideBarStatBox">
                          <h3><i class="fas fa-question-circle"></i>Total Queestions (<?= $total_question?>)</h3>
                        </div>
                        <div class="sideBarStatBox">
                          <h3><i class="fas fa-reply"></i>Total Answers (<?= $answer ?>)</h3>
                        </div>
                        <div class="sideBarStatBox">
                          <h3><i class="fas fa-question"></i>Your asked <?= $user_total_question ?> Questions</h3>
                        </div>
                        <!-- <div class="sideBarStatBox">
                          <h3><i class="fas fa-thumbs-up"></i>Likes (200)</h3>
                        </div> -->
                      </div>   
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
</section>

<!-- MAIN CONTENT SECTION ENDS HERE -->


<!-- FOOTER SECTION STARTS HERE -->
  <?php include 'footer.php'; ?>
<!-- FOOTER SECTION ENDS HERE -->



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/fontawesome.min.js"></script>
    <script src="js/jquery.js"></script>

  </body>
</html>